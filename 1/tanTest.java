package myClass;

import static org.junit.Assert.*;

import org.junit.Test;

public class tanTest {

	@Test
	public void test() {
		double pi=3.1415926;
		my test=new my();
		
		Object output1=test.getTan(0.16666666666*pi); //test tan(30degree)
		System.out.println("tan(pi/6): "+output1);
		assertEquals(0.5773, (double)output1, 0.001);	//tan(1/3* pi)==0.577
		
		Object output2=test.getTan(0.25*pi);	//test tan(45degree)==1
		System.out.println("tan(pi/4): "+output2);
		assertEquals(1, (double)output2, 0.001);
		
		Object output4=test.getTan(0.6666666667*pi);
		System.out.println("tan(1/3*pi): "+output4);
		assertEquals(-1.732, (double)output4, 0.001);	//testing on second quadrant
		
		Object output5=test.getTan(pi);
		System.out.println("tan(pi): "+output5);
		assertEquals(0, (double)output5, 0.001);	//tan(pi)==0
		
		Object output6=test.getTan(0);
		System.out.println("tan(0): "+output6);
		assertEquals(0, (double)output6, 0.001);	//testing spical number 0
		
		Object output7=test.getTan(1.25*pi);
		System.out.println("tan(1.25*pi): "+output7);
		assertEquals(1, (double)output7, 0.001);	//testing on third quadrant
		
		Object output8=test.getTan(3.14159);
		System.out.println("tan(3.14159): "+output8);
		assertEquals(0, (double)output8, 0.001);
	}

}
