package myClass;

import myTri.trilib;

public class my {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double x=0.16666666666666666666*3.1415926;
		my m=new my();
		m.getSin(x);
		m.getCos(x);
		m.getTan(x);
		m.getCot(x);
	}
	
/*	double fabs(double x) {
		if(x>=0) {
			return x;
		}
		else {
			return -x;
		}
	}
	
	double sin (double x){
	    int i = 1;
	    double cur = x;
	    double acc = 1;
	    double fact= 1;
	    double pow = x;
	    while (fabs(acc) > .00000001 &&   i < 100){
	        fact *= ((2*i)*(2*i+1));
	        pow *= -1 * x*x; 
	        acc =  pow / fact;
	        cur += acc;
	        i++;
	    }
	    return cur;

	}*/
	
	public double getSin(double x) {
		trilib t=new trilib(x);
		//System.out.println(t.printSin());
		return t.printSin();
		//return sin(x);
	}
	
	public double getCos(double x) {
		//System.out.println(x);
		trilib t=new trilib(x);
		return t.printCos();
	}
	
	public Object getTan(double x) {
		trilib t=new trilib(x);
		return t.printTan();
	}
	
	public Object getCot(double x) {
		trilib t=new trilib(x);
		return t.printCot();
	}

}
