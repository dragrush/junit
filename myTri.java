package myTri;

public class trilib {

	public double x;
	
	public trilib(double x) {
		this.x=x;
	}
	
	//sin
	
	private double sin() {
		double result=x;
		double tempx=1;
		int n=1;
		double temp=x;
		int sign=-1;
		for(int i=1; tempx>0.0000000000000000000000001; i+=2) {
			temp*=x*x;
			n*=(i+1)*(i+2);
			tempx=temp/n;
			result+=sign*tempx;
			sign*=(-1);
			//System.out.println(result+" "+tempx);
		}
		return result;
	}
	
	//cos
	private double cos() {
		double result=1, tempx=1, temp=1;
		int n=1, sign=-1;
		for(int i=0; tempx>0.0000000000000000000000001; i+=2) {
			temp*=x*x;
			n*=(i+1)*(i+2);
			tempx=temp/n;
			result+=sign*(tempx);
			sign*=-1;
		}
		return result;
	}
	
	//tan
	private double tan() {
		return sin()/cos();
	}
	
	//cot
	private double cot() {
		return 1/tan();
	}
	
	public float printSin() {
		return (float)sin();
	}
	
	public float printCos() {
		return (float)cos();
	}
	
	public float printTan() {
		return (float)tan();
	}
	
	public float printCot() {
		return (float)cot();
	}
}
