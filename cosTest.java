package myClass;

import static org.junit.Assert.*;

import org.junit.Test;

public class cosTest {

	@Test
	public void test() {
		double pi=3.1415926;
		my test=new my();
		
		double output1=test.getCos(0.16666666666*pi); //test cos(30degree)
		System.out.println("cos(pi/6): "+output1);
		assertEquals(0.866, output1, 0.001);	//cos(1/3* pi)==0.866
		
		double output2=test.getCos(0.25*pi);	//test cos(45degree)==0.707
		System.out.println("cos(pi/4): "+output2);
		assertEquals(0.707, output2, 0.001);
		
		assertEquals(test.getCos(0.25*pi), output2, 0.001);	//cos(45d)==sin(45d)
		
		double output4=test.getCos(0.66666666667*pi);
		System.out.println("cos(1/3*pi): "+output4);
		assertEquals(-0.5, output4, 0.001);	//testing on second quadrant
		
		double output5=test.getCos(pi);
		System.out.println("cos(pi): "+output5);
		assertEquals(-1, output5, 0.001);	//cos(pi)==-1
		
		double output6=test.getCos(0);
		System.out.println("cos(0): "+output6);
		assertEquals(1, output6, 0.001);	//testing spical number 0
		
		double output7=test.getCos(1.25*pi);
		System.out.println("cos(1.25*pi): "+output7);
		assertEquals(-0.707, output7, 0.001);	//testing on third quadrant
		
		double output8=test.getCos(3.14159);
		System.out.println("cos(3。14159): "+output8);
		assertEquals(-1, output8, 0.001);
	
	}

}
