package myTri;

public class trilib {

	public double x;
	//double pi=3.1415926;
	//double pih=3.1415927;
	
	public trilib(double x) {
		this.x=x;
	}
	
	double fabs(double x) {
		if(x>=0) {
			return x;
		}
		else {
			return -x;
		}
	}
	
	//sin
	
	private double sin() {
		/*double result=x;
		double tempx=1;
		int n=1;
		double temp=x;
		for(int i=1; fabs(tempx)>0.0000000000000000000000001; i+=2) {
			temp*= -1 *x*x;
			n*=(i+1)*(i+2);
			tempx=temp/n;
			result+=tempx;
			//System.out.println(result+" "+tempx);
		}
		return result;*/
		int i = 1;
	    double result = x;
	    double tempx = 1;
	    double n= 1;
	    double temp = x;
	    while (fabs(tempx) > .00000001 &&   i < 100){
	        n *= ((2*i)*(2*i+1));
	        temp*= -1 * x*x; 
	        tempx=temp/n;
	        result += tempx;
	        i++;
	    }
	    return result;
	}
	
	//cos
	private double cos() {
		double result=1, tempx=1, temp=1;
		double n=1;
		int i=0;
		while(fabs(tempx) > .00000001 &&   i < 100) {
			n*=(2*i+2)*(2*i+1);
			temp*=-1 * x*x;
			tempx=temp/n;
			result+=tempx;
			i++;
		}
		return result;
	}
	
	//tan
	private Object tan() {
		if(cos()!=0) {
			return sin()/cos();
		}
		else {
			return null;
		}
	}
	
	//cot
	private Object cot() {
		if(tan()!=null) {
			return 1/(double)tan();
		}
		else {
			return null;
		}
	}
	
	public double printSin() {
		return sin();
	}
	
	public double printCos() {
		return cos();
	}
	
	public Object printTan() {
		return tan();
	}
	
	public Object printCot() {
		return cot();
	}
}
