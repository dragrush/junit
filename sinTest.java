package myClass;

import static org.junit.Assert.*;

import org.junit.Test;

public class sinTest {

	@Test
	public void test() {
		double pi=3.1415926;
		my test=new my();
		
		double output1=test.getSin(0.16666666666*pi); //test sin(30degree)
		System.out.println("sin(pi/6): "+output1);
		assertEquals(0.5, output1, 0.001);	//sin(1/3* pi)==0.5
		
		double output2=test.getSin(0.25*pi);	//test sin(45degree)==0.707
		System.out.println("sin(pi/4): "+output2);
		assertEquals(0.707, output2, 0.001);
		
		assertEquals(test.getCos(0.25*pi), output2, 0.001);	//sin(45d)==cos(45d)
		
		//double output3=test.getSin(1/2*pi);
		//System.out.println("3: "+output3);
		//assertEquals(1, output3, 0.001);	//sin(pi)==1
		
		double output4=test.getSin(0.6666666667*pi);
		System.out.println("sin(1/3*pi): "+output4);
		assertEquals(0.866, output4, 0.001);	//testing on second quadrant
		
		//double output5=test.getSin(pi);
		//System.out.println("4: "+output4);
		//assertEquals(0, output4, 0.001);	//sin(pi)==0
		
		double output6=test.getSin(0);
		System.out.println("sin(0): "+output6);
		assertEquals(0, output6, 0.001);	//testing spical number 0
		
		double output7=test.getSin(123);
		System.out.println("sin(1.25*pi): "+output7);
		assertEquals(-0.707, output7, 0.001);	//testing on third quadrant
	}

}
